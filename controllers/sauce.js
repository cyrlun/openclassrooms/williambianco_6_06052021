// in controllers/sauce.js


const Sauce = require('../models/sauce');
const fs = require('fs');

exports.createSauce = (req, res, next) => {
    const sauceObject = JSON.parse(req.body.sauce);
    delete sauceObject._id;
    const sauce = new Sauce({
        ...sauceObject,
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    });
    console.log(JSON.parse(JSON.stringify(req.body)));
    sauce.save()
        .then(() => {
            res.status(201).json({
                message: 'Sauce enregistrée!'
            });
        })
        .catch(
            (error) => {

                res.status(400).json({

                    error: error
                });
            }
        );
};

exports.getOneSauce = (req, res, next) => {
    Sauce.findOne({
            _id: req.params.id
        })
        .then(sauce => res.status(200).json(sauce))
        .catch(error => res.status(404).json({
            error
        }));
}

exports.modifySauce = (req, res, next) => {
    const sauceObject = req.file ? {
        ...JSON.parse(req.body.sauce),
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    } : {
        ...req.body
    };
    Sauce.updateOne({
            _id: req.params.id
        }, {
            ...sauceObject,
            _id: req.params.id
        })
        .then(() => res.status(200).json({
            message: 'Sauce modifiée !'
        }))
        .catch(error => res.status(400).json({
            error
        }));
};
exports.likeSauce = (req, res, next) => {
    Sauce.findOne({
            _id: req.params.id
        })
        .then(sauce => {         
            const addLike = id => sauce.usersLiked.push(id)
            const addDislike = id => sauce.usersDisliked.push(id)
            const isLikedIndex = sauce.usersLiked.indexOf(req.body.userId) 
            const isDislikedIndex = sauce.usersDisliked.indexOf(req.body.userId) 
            // si nouveau like
            if (req.body.like == 1) {
                addLike(req.body.userId)
                sauce.likes = sauce.usersLiked.length                
            }
            if (req.body.like == -1) {
                addDislike(req.body.userId)
                sauce.dislikes = sauce.usersDisliked.length
            }
            if(req.body.like == 0){
                if (isLikedIndex > -1) {
                    sauce.usersLiked.splice(isLikedIndex,1)
                    sauce.likes = sauce.usersLiked.length                    
                }
                if (isDislikedIndex > -1) {
                    sauce.usersDisliked.splice(isDislikedIndex,1)
                    sauce.dislikes = sauce.usersDisliked.length
                }
            }
            sauce.save()    
        })
        .then(() => res.status(200).json({
            message: `ok!`
        }))
        .catch(error => res.status(400).json({
            error
        }))
}
exports.deleteSauce = (req, res, next) => {

    Sauce.findOne({
            _id: req.params.id
        })
        .then(sauce => {
            const filename = sauce.imageUrl.split('/images/')[1];
            fs.unlink(`images/${filename}`, () => {
                Sauce.deleteOne({
                        _id: req.params.id
                    })
                    .then(() => res.status(200).json({
                        message: 'Sauce supprimée'
                    }))
                    .catch(error => res.status(400).json({
                        error
                    }));
            });
        })
        .catch(error => res.status(500).json({
            error
        }));

};

exports.getAllSauces = (req, res, next) => {
    Sauce.find()
        .then(sauce => res.status(200).json(sauce))
        .catch(error => res.status(400).json({
            error
        }));
}